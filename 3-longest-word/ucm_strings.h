#ifndef UCM_STRINGS_H
#define UCM_STRINGS_H

#include<iostream>
#include <string>

/*
    Create a function that finds the largest word in a string.
*/

bool isWordEnd(char letter){
    if (letter == ' ' || letter == ',' || letter == '.' || letter == '!' || letter == '?'){
        return true;
    }
    else{
        return false;
    }
}


std::string longestWord(std::string sen){
    std::string word = "";
    std::string longest = "";
    for (int i = 0; i < sen.length(); i++){
        if (!isWordEnd(sen[i])){
            word = word + sen[i];
        }
        else{
            if (word.length() > longest.length()){
                longest = word;
            }
            word = "";
        }
    }

    if (word.length() > longest.length()){
        longest = word;
    }

    return longest;
}

#endif