#include <iostream>
#include "ucm_strings.h"
using namespace std;

int main() {

    string text = "I like apples, and other fruit";
    string word = longestWord(text);

    cout << "Longest word: " << word << endl;

    return 0;
}
